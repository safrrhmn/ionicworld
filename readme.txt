#############################
PROJECT SETUP INSTTRUCTIONS
#############################


###################
Install Node.js
###################

Simply download the MSI Installer from nodejs .org - then launch it and follow the instructions.
Make sure to download v4.4.0 LTS.
To confirm that Node.js and the Node Package Manager have been installed correctly open a
Command Prompt window and type
node -v
and
npm -v
and should see their respective version numbers.

#####################

Install the Java Development Kit (JDK)
Install Java Development Kit (JDK) 7 or later.
When installing on Windows you also need to set JAVA_HOMEEnvironment Variable according to JDK
installation path (for example, C:\Program Files\Java\jdk1.7.0_75).

###########################
Install the Android SDK
###########################

Install the Android Stand­alone SDK Tools or Android Studio. Proceed with AndroidStudioif you
plan developing new Cordova for Android plugins or using native tools to run and debug Android
platform. Otherwise, AndroidStand-aloneSDKToolsare enough to build and deploy Android
application.
Detailed installation instructions are available as part of installation links above.
For Cordova command­line tools to work, or the CLI that is based upon them, you need to include
the SDK's toolsand platform-toolsdirectories in your PATH. On a Mac or Linux, you can use a text
editor to create or modify the ~/.bash_profilefile, adding a line such as the following, depending
on where the SDK installs:
exportPATH=${PATH}:/Development/android-sdk/platform-tools:/Development/android-sdk/tools
This line in ~/.bash_profileexposes these tools in newly opened terminal windows. If your terminal
window is already open in OSX, or to avoid a logout/login on Linux, run this to make them available
in the current terminal window:
$source~/.bash_profile
To modify the PATHenvironment on Windows:
1. Click on the Start​menu in the lower­left corner of the desktop, right­click on Computer​, then select Properties​.
2. Select Advanced System Settings​in the column on the left.
3. In the resulting dialog box, press Environment Variables​.
4. Select the PATH​variable and press Edit​.
5. Append the following to the PATHbased on where you installed the SDK, for example:
6. ;C:\Development\android-sdk\platform-tools;C:\Development\android-sdk\tools
7. Save the value and close both dialog boxes.


########################
Install SDK Packages
########################

Open Android SDK Manager (for example, via terminal: android) and install:
1. Android 5.1.1 (API 22) platform SDK
2. Android SDK Build­tools version 19.1.0 or higher
3. Android Support Repository (Extras)git

########################
Setting up ionic
########################
go to the command line and run:

1. npm install -g cordova
  --> This will install cordova and all the required dependencies for cordova (Sweet!!!)
  --> Check by running command cordova -v to check if it is installed

2. npm install -g ionic
  --> check the version by running ionic -v

3. You will also need bower for third party JS libraries
   Run npm install -g bower
    --> check the version to make sure

4. Install gulp, it is required by ionic to build some parts of the project
   Run npm install -g gulp
   --> check by running gulp -v

SKIPPING THE ANROID AND IOS part, let me know if you need them and I will add them
#################################   
STARTING THE TEMPLATE APPLICATION
#################################
I guess you could just clone this, but if you want this to be a tutorial I can put the instructions here